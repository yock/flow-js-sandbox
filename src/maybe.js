const optionalParameter = (foo) => {
  return foo * foo;
}

optionalParameter(6);
optionalParameter();
